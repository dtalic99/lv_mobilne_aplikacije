package com.example.lv10


import androidx.room.Database
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = [Work::class], version = 1)
@TypeConverters(WorkTypeConverters::class)
abstract class WorkDatabase : RoomDatabase() {
    abstract fun workDao():WorkDao
}