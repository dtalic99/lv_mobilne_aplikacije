package com.example.lv10
import androidx.lifecycle.ViewModel


class WorkListViewModel : ViewModel() {
    private val workRepository = WorkRepository.get()
    val jobs = workRepository.getJobs()
}