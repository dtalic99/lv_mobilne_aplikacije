package com.example.lv10

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import java.util.*;

@Entity
data class Work (@PrimaryKey val id: UUID,
                 var title: String = "",
                 var date: Date = Date(),
                 var IsDone: Boolean = false)