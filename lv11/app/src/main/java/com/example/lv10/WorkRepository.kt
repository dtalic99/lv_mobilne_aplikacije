package com.example.lv10

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.Executors

private const val DATABASE_NAME = "work-database"
private val workDao = database.workDao()
private val executor = Executors.newSingleThreadExecutor()

class WorkRepository  private constructor(context: Context) {

    private val database: WorkDatabase = Room.databaseBuilder(
        context.applicationContext,
        WorkDatabase::class.java,
        DATABASE_NAME



    ).build()



    fun getJobs(): List<Work> = workDao.getJobs()
    fun getJob(id: UUID): Work? = workDao.getJob(id)

    private val workDao = database.workDao()



    companion object {
        private var INSTANCE: WorkRepository? = null

        fun initialize(context: Context) {
            if(INSTANCE == null) {
                INSTANCE = WorkRepository(context)
            }
        }

        fun get(): WorkRepository {
            return INSTANCE ?: throw IllegalStateException("WorkRepository must be initialized!")
        }

    }
}