package com.example.lv10

import androidx.room.TypeConverter
import java.util.*


class WorkTypeConverters {

    @TypeConverter
    fun fromDate(date: Date?): Long? {
        return date?.time;
    }

    @TypeConverter
    fun toDate(milisSinceWork: Long?): Date? {
        return  milisSinceWork?.let{
            Date(it);
        }
    }

    @TypeConverter
    fun fromUUID(uuid: UUID?): String? {
        return uuid?.toString();
    }

    @TypeConverter
    fun toUUID(uuid: String?): UUID? {
        return UUID.fromString(uuid);
    }
}