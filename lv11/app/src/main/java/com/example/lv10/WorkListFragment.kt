package com.example.lv10

class WorkListFragment {

    private var adapter: WorkAdapter? = WorkAdapter(emptyList())
    ...
    workRecyclerView.layoutManager = LinearLayoutManager(context)
    workRecyclerView.adapter = adapter
    return view;
}