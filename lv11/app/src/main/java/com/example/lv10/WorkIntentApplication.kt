package com.example.lv10

import android.app.Application

class WorkIntentApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        WorkRepository.initialize(this)
    }

}