package com.example.lv10

import androidx.lifecycle.LiveData
import java.util.*
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update


@Dao
interface WorkDao {
    @Query("SELECT * FROM work")
    fun getJobs(): LiveData<List<Work>>
    @Query("SELECT * FROM work WHERE id=(:id)")
    fun getJob(id: UUID) : LiveData<Work?>
    @Update
    fun updateJob(work: Work)
    @Insert
    fun addJob(work: Work)
}