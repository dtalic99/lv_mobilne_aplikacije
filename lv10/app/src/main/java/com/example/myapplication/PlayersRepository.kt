package com.example.myapplication

import android.content.Context
import java.lang.IllegalStateException

class PlayersRepository private constructor(context: Context) {

    companion object {

        private var INSTANCE: PlayersRepository? = null

        fun initialize(context: Context){
            if(INSTANCE == null){
                INSTANCE = PlayersRepository(context)
            }
        }

        fun get(): PlayersRepository{
            return INSTANCE ?: throw IllegalStateException("PlayersRepository must be initialized")
        }
    }
}