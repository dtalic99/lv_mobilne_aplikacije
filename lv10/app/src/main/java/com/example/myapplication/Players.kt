package com.example.myapplication

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Players (@PrimaryKey val id: UUID = UUID.randomUUID(),
        var name: String = "",
        var surname: String = "",
        var isDone: Boolean = false)
