package com.example.myapplication

import androidx.room.Dao
import androidx.room.Query
import com.example.myapplication.Players
import java.util.*

@Dao
interface PlayersDao {

    @Query("SELECT * FROM Players")
    fun getPlayers(): List<Players>

    @Query("SELECT * FROM Players WHERE id=(:id)")
    fun getPlayer(id: UUID) : Players


}