package com.example.myapplication.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.myapplication.Players
import com.example.myapplication.PlayersDao

@Database(entities = [Players::class], version = 1)
@TypeConverters(PlayersTypeConverters::class)
abstract class PlayersDatabase:RoomDatabase() {
    abstract fun playersDao():PlayersDao
}