package com.example.counter2



import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var counter = 0;
    private lateinit var tvCounter : TextView
    private lateinit var btnIncrease : Button
    private lateinit var btnNoviActivity : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvCounter = findViewById(R.id.tvCounter)
        btnIncrease = findViewById(R.id.btnIncrease)
        btnNoviActivity = findViewById(R.id.btnNoviActivity)

        btnIncrease.setOnClickListener {
            counter++
            tvCounter.text = counter.toString();
        }
        if (savedInstanceState != null) {
            counter = savedInstanceState.getInt("count")
            tvCounter.text = counter.toString();

        }
        btnNoviActivity.setOnClickListener(){
            noviActivity(tvCounter);
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt("count", counter)
    }

    fun sendMessage(view: View) {
        val intent = Intent(this, MainActivity2::class.java).apply {}
        startActivity(intent)
    }
    fun noviActivity(view: View) {
        val intent = Intent(this, MainActivity2::class.java).apply {}
        startActivity(intent)
    }


}