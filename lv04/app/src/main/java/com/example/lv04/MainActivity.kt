package com.example.lv04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

private const val TAG = "MainActivity";
class MainActivity : AppCompatActivity() {
    private lateinit var btnIncrement : Button;
    private lateinit var txtCounter : TextView;
    private val counterViewModel : CounterViewModel by lazy {
        ViewModelProviders.of(this).get(CounterViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnIncrement = findViewById(R.id.btnIncrement);
        txtCounter = findViewById(R.id.txtCounter);

        onClickHandler();
    }

    private fun onClickHandler() {
        btnIncrement.setOnClickListener {
            counterViewModel.incrementCounter();
            showCounterStatus(counterViewModel.getCurrentCounterState);
        }
    }

    private fun showCounterStatus(number : Int) {
        txtCounter.text = number.toString();
    }
}