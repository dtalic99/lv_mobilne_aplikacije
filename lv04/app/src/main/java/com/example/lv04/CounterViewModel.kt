package com.example.lv04

import android.os.Bundle
import android.util.Log;
import androidx.lifecycle.ViewModel;

private const val TAG = "CounterViewModel";
class CounterViewModel : ViewModel() {
    private var counter : Counter = Counter(0);

    val getCurrentCounterState: Int
        get() = counter.number;

    fun incrementCounter() {
        counter.number++;
    }
}