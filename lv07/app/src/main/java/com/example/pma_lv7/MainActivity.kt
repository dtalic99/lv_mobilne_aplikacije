package com.example.pma_lv7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

private lateinit var tvShow : TextView
private lateinit var imgShow : ImageView
private lateinit var radioBtn: RadioButton
private lateinit var cbShow : CheckBox
private lateinit var tggleBtn : ToggleButton
private lateinit var btnPrint : Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvShow = findViewById(R.id.tvEdit)
        imgShow = findViewById(R.id.imgShow)
        radioBtn = findViewById(R.id.radBtn)
        cbShow = findViewById(R.id.cbShow)
        tggleBtn = findViewById(R.id.tggleBtnShow)
        btnPrint = findViewById(R.id.btnPrint)


        btnPrint.setOnClickListener{
            Toast.makeText(this, tvShow.text, Toast.LENGTH_LONG ).show()
            if(radioBtn.isChecked)
                Toast.makeText(this, "Radio gumb je pritisnut", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Radio gumb nije pritisnut", Toast.LENGTH_SHORT).show()
            if(cbShow.isChecked)
                Toast.makeText(this, "Checkbox je oznacen", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Checkbox je odznacen", Toast.LENGTH_SHORT).show()
            if(tggleBtn.isChecked)
                Toast.makeText(this, "Toggle gumb je upaljen", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Toggle gumb je ugasen", Toast.LENGTH_SHORT).show()
        }

        tggleBtn.setOnClickListener{
            if(tggleBtn.isChecked)
                Toast.makeText(this, "Toggle gumb je upaljen", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Toggle gumb je ugasen", Toast.LENGTH_SHORT).show()
        }

        radioBtn.setOnClickListener{
            Toast.makeText(this, "Radio gumb je pritisnut", Toast.LENGTH_SHORT).show()
        }

        cbShow.setOnClickListener{
            if(cbShow.isChecked)
                Toast.makeText(this, "Checkbox je oznacen", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Checkbox je odznacen", Toast.LENGTH_SHORT).show()
        }
    }
}