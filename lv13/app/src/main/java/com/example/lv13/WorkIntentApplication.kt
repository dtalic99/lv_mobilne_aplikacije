package com.example.lv13

import android.app.Application
import com.example.lv13.WorkRepository

class WorkIntentApplication : Application(){

    override fun onCreate(){
        super.onCreate()
        WorkRepository.initialize(this)
    }
}