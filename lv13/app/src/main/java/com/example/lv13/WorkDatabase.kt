package com.example.lv13

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.lv13.Work

@Database(entities = [Work::class], version = 1)
@TypeConverters(WorkTypeConverters::class)
abstract class WorkDatabase :RoomDatabase(){
    abstract fun workDao():WorkDao
}