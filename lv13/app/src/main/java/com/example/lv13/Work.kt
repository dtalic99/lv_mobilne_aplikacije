package com.example.lv13

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

data class Work(@PrimaryKey val id: UUID = UUID.randomUUID(),
            var title: String = "",
            var date: Date = Date(),
            var isDone: Boolean = false)
