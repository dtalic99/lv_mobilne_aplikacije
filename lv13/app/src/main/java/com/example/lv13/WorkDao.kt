package com.example.lv13

import androidx.core.location.LocationRequestCompat
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.lv13.Work
import java.util.*

@Dao
interface WorkDao {

    @Query("SELECT * FROM work")
    fun getJobs():LiveData<List<Work>>

    @Query("SELECT * FROM work WHERE id=(:id)")
    fun getJob(id: UUID) : LiveData<Work?>
}