package com.example.lv13

import android.content.Context
import androidx.lifecycle.LiveData
import java.lang.IllegalStateException
import androidx.room.Room
import com.example.lv13.WorkDatabase
import java.util.*

private const val DATABASE_NAME = "work-database"

class WorkRepository private constructor(context: Context) {

    private val database: WorkDatabase = Room.databaseBuilder(context.applicationContext, WorkDatabase::class.java, DATABASE_NAME).build()
    private val workDao = database.workDao()
    fun getJobs():LiveData<List<Work>> = workDao.getJobs()
    fun getJob(id: UUID): LiveData<Work?> = workDao.getJob(id)


    companion object {

        private var INSTANCE: WorkRepository? = null

        fun initialize(context: Context){
            if(INSTANCE == null){
                INSTANCE = WorkRepository(context)
            }
        }

        fun get(): WorkRepository{
            return INSTANCE ?: throw IllegalStateException("PlayersRepository must be initialized")
        }
    }
}