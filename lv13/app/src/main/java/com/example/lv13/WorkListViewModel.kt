package com.example.lv13

import androidx.lifecycle.ViewModel
import com.example.lv11.WorkRepository

class WorkListViewModel : ViewModel(){



    private val workRepository = WorkRepository.get()
    val jobs = workRepository.getJobs()
}