package com.example.lv13

import android.os.Bundle
import android.provider.Settings.System.DATE_FORMAT
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import java.lang.String.format
import java.text.DateFormat

class WorkFragment : Fragment() {
    private lateinit var work: Work
    private lateinit var titleField: EditText
    private lateinit var dateButton: Button
    private lateinit var doneCheckBox: CheckBox

    private var adapter: WorkAdapter? =WorkAdapter(emptyList())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        work = Work()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_work, container, false)
        titleField = view.findViewById(R.id.work_title) as EditText
        dateButton = view.findViewById(R.id.work_date) as Button
        doneCheckBox = view.findViewById(R.id.work_done) as CheckBox
        dateButton.apply {
            text = work.date.toString()
            isEnabled = false
        }
        return view
    }

    private fun getWorkReport(): String {
        val doneString = if(work.isDone){
            getString(R.string.work_report_done)
        } else {
            getString(R.string.work_report_not_finished)
        }
        val dateString = DateFormat.format(DATE_FORMAT, work.date).toString()
        return getString(R.string.work_report, work.title, dateString, doneString)
    }

    override fun onStart() {
        super.onStart()
        //val titleWatcher: TextWatcher
        val titleWatcher = object : TextWatcher {
            override fun beforeTextChanged(
                sequence: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                sequence: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
                work.title = sequence.toString()
            }

            override fun afterTextChanged(sequence: Editable?) {
                // This one too
            }
        }
                titleField.addTextChangedListener(titleWatcher)
                doneCheckBox.apply {
                    setOnCheckedChangeListener { _, isChecked ->
                        work.isDone = isChecked
                    }
                }


    }
}