package com.example.lv02


import android.util.Log
import androidx.lifecycle.ViewModel

class QuestionViewModel() : ViewModel() {
    private  var questionIndex : Int = 0;
    private  var questions = ArrayList<Question>();

    fun initializeQuestions() {
        questions.add(
            Question(
                "Java is a programming language which is 50 years old?", false
            )
        );
        questions.add(
            Question(
                "VUB is actually an achronym for \"Veleuciliste u Bjelovaru\"?", true
            )
        );
        questions.add(
            Question(
                "Linux is an operating system?", false
            )
        );
        questions.add(
            Question(
                "Windows 10 is the last version of windows which will be periodically updated?", true
            )
        );
        questions.add(
            Question(
                "Shiba Inu is a Japanese dog breed?", true
            )
        );
    }

    fun showQuestion(index: Int): String {
        return questions[questionIndex].question;
    }

    fun isQuestionTrue(index: Int): Boolean {
        return questions[questionIndex].isItTrue;
    }

    fun incrementQuestionIndex() {
        questionIndex++;
    }

    val getQuestionIndex
        get() = questionIndex;
}