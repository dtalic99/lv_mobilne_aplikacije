@file:Suppress("DEPRECATION")

package com.example.lv02

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders


private const val TAG = "MainActivity";

class MainActivity : AppCompatActivity() {
    private lateinit var txtQuestion : TextView;
    private lateinit var btnTrue : Button;
    private lateinit var btnFalse : Button;
    private lateinit var btnNext : Button;
    private val questionViewModel : QuestionViewModel by lazy {
        ViewModelProviders.of(this).get(QuestionViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        btnTrue = findViewById(R.id.btnTrue);
        btnFalse = findViewById(R.id.btnFalse);
        btnNext = findViewById(R.id.btnNext);
        txtQuestion = findViewById(R.id.txtQuestion);

        onClickListenerInit();
        questionViewModel.initializeQuestions();
        showQuestions(0);
    }

    private fun onClickListenerInit() {
        btnTrue.setOnClickListener {
            showToastMessage(questionViewModel.isQuestionTrue(questionViewModel.getQuestionIndex));
        }

        btnFalse.setOnClickListener {
            showToastMessage(!questionViewModel.isQuestionTrue(questionViewModel.getQuestionIndex));
        }

        btnNext.setOnClickListener {
            questionViewModel.incrementQuestionIndex();
            showQuestions(questionViewModel.getQuestionIndex);
        }
    }

    private fun showToastMessage(answer: Boolean) {
        if (answer) {
            Toast.makeText(this@MainActivity, getString(R.string.msg_true), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this@MainActivity, getString(R.string.msg_false), Toast.LENGTH_SHORT).show()
        }
    }

    private fun showQuestions(index: Int) {
        txtQuestion.text = questionViewModel.showQuestion(questionViewModel.getQuestionIndex).toString();
    }

}