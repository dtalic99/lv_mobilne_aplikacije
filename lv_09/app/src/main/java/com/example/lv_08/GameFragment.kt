package com.example.lv_08

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.fragment.app.Fragment

class GameFragment : Fragment() {
    private lateinit var game: Game;
    private lateinit var titleField: EditText;
    private lateinit var priceField: EditText;
    private lateinit var isFinishedByPlayer: CheckBox
    private lateinit var btnSubmit: Button;
    private lateinit var titleWatcher: TextWatcher;
    private lateinit var priceWatcher: TextWatcher;
    private lateinit var stringBuilder: StringBuilder;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        game = Game();
        stringBuilder = StringBuilder()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_game, container, false);
        grabAllUIelements(view);
        return view;
    }

    override fun onStart() {
        super.onStart()

        addTextWatchers();
        setCheckBoxListener();
        setSubmitBtnListener()
    }

    fun grabAllUIelements(view: View) {
        titleField = view.findViewById(R.id.editTxtGameTitle) as EditText;
        priceField = view.findViewById(R.id.editTxtGamePrice) as EditText;
        isFinishedByPlayer = view.findViewById(R.id.chkBoxIsGameFinished) as CheckBox;
        btnSubmit = view.findViewById(R.id.btnSubmit) as Button;
    }

    fun addTextWatchers() {
        titleWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                game.title = s.toString();
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }

        priceWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                game.price = s.toString().toDouble();
            }

            override fun afterTextChanged(s: Editable?) {

            }
        }

        titleField.addTextChangedListener(titleWatcher)

        priceField.addTextChangedListener(priceWatcher)
    }

    fun setCheckBoxListener() {
        isFinishedByPlayer.apply {
            setOnCheckedChangeListener{
                _, isChecked -> game.isFinishedByPlayer = isChecked
            }
        }
    }

    fun getDataFromAllInputs() {
        stringBuilder
            .append("\nGame title: " + game.title + "\n")
            .append("Game price: " + game.price.toString() + "\n")
            .append("Is game finished by player? " + game.isFinishedByPlayer.toString() + "\n")
    }

    fun printDataToConsole(stringBuilder: StringBuilder) {
        Log.d("Printing submitted data to log", stringBuilder.toString())
    }

    fun setSubmitBtnListener() {
        btnSubmit.setOnClickListener {
            getDataFromAllInputs()
            printDataToConsole(stringBuilder)
        }
    }
}