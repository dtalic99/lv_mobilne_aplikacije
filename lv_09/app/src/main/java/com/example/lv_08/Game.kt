package com.example.lv_08

import java.util.*

data class Game(
    val id: UUID = UUID.randomUUID(),
    var title: String = "",
    var price: Double = 0.01,
    var isFinishedByPlayer: Boolean = false
) {
}