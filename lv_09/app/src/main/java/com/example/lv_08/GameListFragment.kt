package com.example.lv_08

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class GameListFragment : Fragment() {
    private lateinit var gameRecyclerView : RecyclerView;
    private var adapter : GameAdapter? = null;
    private val gameListViewModel : GameListViewModel by lazy {
        ViewModelProviders.of(this).get(GameListViewModel::class.java);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("daaddd", "Total jobs: ${gameListViewModel.games.size}")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_game_list, container, false)
        gameRecyclerView = view.findViewById(R.id.game_recycler_view) as RecyclerView
        gameRecyclerView.layoutManager = LinearLayoutManager(context)
        updateUI()
        return view;
    }

    private fun updateUI() {
        val games = gameListViewModel.games;
        adapter = GameAdapter(games)
        gameRecyclerView.adapter = adapter;
    }

    private inner class GameHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private lateinit var game: Game
        private val gameTitleTextView: TextView = view.findViewById(R.id.game_title);
        private val gamePriceTextView: TextView = view.findViewById(R.id.game_price);

        init {
            view.setOnClickListener(this)
        }

        fun bind(game: Game) {
            this.game = game
            gameTitleTextView.text = game.title
            gamePriceTextView.text = game.price.toString()
        }

        override fun onClick(v: View?) {
            Toast.makeText(context, "${game.title} pressed", Toast.LENGTH_SHORT).show()
        }
    }

    private inner class GameAdapter(val games: List<Game>) : RecyclerView.Adapter<GameHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameHolder {
            val view = layoutInflater.inflate(R.layout.list_item_game, parent, false);
            return GameHolder(view);
        }

        override fun getItemCount(): Int {
            return games.count();
        }

        override fun onBindViewHolder(holder: GameHolder, position: Int) {
            val game = games[position]
            holder.bind(game)
        }

    }

    companion object {
        fun newInstance() : GameListFragment {
            return GameListFragment()
        }
    }
}