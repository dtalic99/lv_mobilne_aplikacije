package com.example.lv_08

import androidx.lifecycle.ViewModel

class GameListViewModel : ViewModel() {
    val games = mutableListOf<Game>();

    init {
        for (i in 0 until 100) {
            val game = Game()
            game.title = "Game placeholder"
            game.price = i + 25.25;
            games.add(game)
        }
    }
}